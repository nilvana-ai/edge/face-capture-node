const axios = require('axios')
const FormData = require('form-data')

module.exports = function (RED) {
  "use strict"
  var isUtf8 = require('is-utf8')

  function MQTTInNode (n) {
    RED.nodes.createNode(this, n)
    this.topic = n.topic
    this.qos = parseInt(n.qos)
    if (isNaN(this.qos) || this.qos < 0 || this.qos > 2) {
      this.qos = 1
    }
    this.broker = n.broker
    this.brokerConn = RED.nodes.getNode(this.broker)
    if (!/^(#$|(\+|[^+#]*)(\/(\+|[^+#]*))*(\/(\+|#|[^+#]*))?$)/.test(this.topic)) {
      return this.warn(RED._("mqtt.errors.invalid-topic"))
    }
    this.datatype = n.datatype || "utf8"
    this.remoteIP = n.remoteIP
    if (!/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i.test(this.remoteIP)) {
      return this.warn(RED._("mqtt.errors.invalid-remote-ip"))
    }
    var node = this
    if (this.brokerConn) {
      this.status({
        fill: "red",
        shape: "ring",
        text: "node-red:common.status.disconnected"
      })
      if (this.topic) {
        node.brokerConn.register(this)
        this.brokerConn.subscribe(this.topic, this.qos, function (topic, payload, packet) {
          if (node.datatype === "buffer") {
            // payload = payload;
          } else if (node.datatype === "base64") {
            payload = payload.toString('base64')
          } else if (node.datatype === "utf8") {
            payload = payload.toString('utf8')
          } else if (node.datatype === "json") {
            if (isUtf8(payload)) {
              payload = payload.toString()
              try {
                payload = JSON.parse(payload)
              } catch (e) {
                node.error(RED._("mqtt.errors.invalid-json-parse"), {
                  payload: payload,
                  topic: topic,
                  qos: packet.qos,
                  retain: packet.retain
                })
                return
              }
            } else {
              node.error((RED._("mqtt.errors.invalid-json-string")), {
                payload: payload,
                topic: topic,
                qos: packet.qos,
                retain: packet.retain
              })
              return
            }
          } else {
            if (isUtf8(payload)) {
              payload = payload.toString()
            }
          }
          var msg = {
            topic: topic,
            payload: payload,
            qos: packet.qos,
            retain: packet.retain
          }
          node.image = payload
          if ((node.brokerConn.broker === "localhost") || (node.brokerConn.broker === "127.0.0.1")) {
            msg._topic = topic
          }

          capture(msg, node).then(res => {
            msg.payload = res.data
            msg.image = node.image
            node.status({
              fill: "green",
              shape: "dot",
              text: "node-red:common.status.connected"
            })
            node.send(msg)
          }).catch(err => {
            console.warn(err)
            node.status({
              fill: "red",
              shape: "dot",
              text: "face-recognition.apiError"
            })
          })
        }, this.id)
        if (this.brokerConn.connected) {
          node.status({
            fill: "green",
            shape: "dot",
            text: "node-red:common.status.connected"
          })
        }
      } else {
        this.error(RED._("mqtt.errors.not-defined"))
      }

      // close event
      this.on('close', function (removed, done) {
        if (node.brokerConn) {
          node.brokerConn.unsubscribe(node.topic, node.id, removed)
          node.brokerConn.deregister(node, done)
        }
      })

    } else {
      this.error(RED._("mqtt.errors.missing-config"))
    }
  }

  function capture (msg, node) {
    node.status({
      fill: "yellow",
      shape: "dot",
      text: "face-recognition.inRecognition"
    })
    const {
      remoteIP
    } = node
    const image_base64 = msg.payload.split(',').length > 1 ? msg.payload.split(',')[1] : msg.payload
    const imgFile = Buffer.from(image_base64, 'base64')
    let form = new FormData()
    form.append('img_file', imgFile, {
      filename: 'ready_for_detect.jpg'
    })
    return axios.post(`http://${remoteIP}:50001/v1/face-reader`, form, {
      headers: form.getHeaders()
    })
  }
  RED.nodes.registerType("face-recognition", MQTTInNode)
}
