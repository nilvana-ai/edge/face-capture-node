# Face capture node

## Get Started

```shell
npm install --save  https://gitlab.com/nilvana-ai/edge/face-capture-node.git
```

## Copyright and license

Copyright inwinSTACK Inc. under [the Apache 2.0 license](LICENSE.md).
